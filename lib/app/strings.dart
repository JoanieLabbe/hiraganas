import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class Strings {
  static const values = {
    "en": {
      "title": "Hiraganas - EN",
      "learnWidget": "Learn",
      "trainWidget": "Train",
    },
    "fr": {
      "title": "Hiraganas - FR",
      "learnWidget": "Apprentissage",
      "trainWidget": "Entrainement",
    }
  };

  final Map<String, String> _values;

  String get title => _values["title"];

  String get learnWidget => _values["learnWidget"];

  String get trainWidget => _values["trainWidget"];

  Strings(Locale locale) : _values = values[locale.languageCode];

  Strings.of(BuildContext context) : this(Localizations.localeOf(context));
}

class AppLocalizationsDelegate extends LocalizationsDelegate<Strings> {
  const AppLocalizationsDelegate._internal();

  static const AppLocalizationsDelegate delegate =
      AppLocalizationsDelegate._internal();

  @override
  bool isSupported(Locale locale) =>
      Strings.values.containsKey(locale.languageCode);

  @override
  Future<Strings> load(Locale locale) =>
      SynchronousFuture<Strings>(Strings(locale));

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
