import 'dart:math';

import '../grapheme/graphemeModel.dart';
import 'config.dart';

class ConfigExtension {
  static Random _rnd = Random();

  static int get _length => Config.graphemes.length;

  static int get _randomIndex => _rnd.nextInt(_length);

  // Obtient aléatoirement un graphème de la liste
  static GraphemeModel getRandomGrapheme() {
    return Config.graphemes[_randomIndex];
  }

  // Crée la liste des choix de réponses (une bonne réponse ainsi qu'un nombre n de mauvaises réponses),
  // puis les positionne de manière aléatoire
  static List<String> getRandomResponses(int quantity, String goodAnswer) {
    List<String> possibleAnswers = List();
    possibleAnswers.add(goodAnswer);
    for (int i = 0; i < quantity - 1; i++) {
      String temp = getRandomGrapheme().translation;
      while (possibleAnswers.contains(temp)) {
        temp = getRandomGrapheme().translation;
      }
      possibleAnswers.add(temp);
    }
    possibleAnswers.shuffle();
    return possibleAnswers;
  }
}
