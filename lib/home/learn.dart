import 'package:flutter/material.dart';
import 'package:tp3_prof/app/config.dart';

class LearnCards extends StatelessWidget {
  const LearnCards() : super();

  @override
  Widget build(BuildContext context) {

    final items = Config.graphemes;

    int _getColumnAmount() {
      if (MediaQuery.of(context).orientation == Orientation.landscape) {
        return 3;
      } else {
        return 2;
      }
    }

    return Scrollbar(
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: _getColumnAmount()),
        itemCount: items.length,
        itemBuilder: (_, i) {
          return new Card(
            child:Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("${items[i].symbol}",
                        style: Theme.of(context).textTheme.headline1),
                  Text("${items[i].translation}"),
            ]
            ),
          );
        },
      ),
    );

  }
}