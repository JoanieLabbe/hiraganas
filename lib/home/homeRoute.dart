import 'package:flutter/material.dart';
import 'package:tp3_prof/app/strings.dart';
import 'package:tp3_prof/home/training.dart';
import 'learn.dart';

class HomeRoute extends StatefulWidget {
  static const routeName = "/home";

  @override
  HomeRouteState createState() => HomeRouteState();
}

class HomeRouteState extends State<HomeRoute> {
  int _selectedIndex = 0;
  var _pageController = PageController();
  static const List<Widget> _widgetOptions = <Widget>[LearnCards(), Training()];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.animateToPage(_selectedIndex,
          duration: Duration(milliseconds: 200), curve: Curves.linear);
    });
  }

  void _onSwiped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final strings = Strings.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.title),
      ),
      body: PageView(
        children: _widgetOptions,
        onPageChanged: _onSwiped,
        controller: _pageController,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: strings.learnWidget,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.create),
            label: strings.trainWidget,
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
