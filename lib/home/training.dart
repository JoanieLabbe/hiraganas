import 'package:flutter/material.dart';
import 'package:tp3_prof/app/configExtension.dart';
import '../grapheme/graphemeModel.dart';

extension Answer on GraphemeModel {
  bool isGoodAnswer(String answer) {
    return this.translation == answer;
  }
}

class Training extends StatefulWidget {
  const Training() : super();

  @override
  _TrainingState createState() => _TrainingState();
}

class _TrainingState extends State<Training> {
  GraphemeModel _grapheme;
  List<String> _responses;
  List<String> _error = List<String>();
  final double _margin = 4;

  _TrainingState() : super() {
    _updateGraphemes();
  }

  void _updateGraphemes() {
    _grapheme = ConfigExtension.getRandomGrapheme();
    _responses = ConfigExtension.getRandomResponses(3, _grapheme.translation);
    _error.clear();
  }

  void _onClick(String answer) {
    if (_grapheme.isGoodAnswer(answer)) {
      setState(() {
        _updateGraphemes();
      });
    } else {
      setState(() {
        _error.add(answer);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          child: Card(
              margin: EdgeInsets.all(_margin),
              child: FittedBox(
                fit: BoxFit.fitHeight,
                child: Text(
                  _grapheme.symbol,
                ),
              )),
        ),
        _buildButton(context, _responses[0]),
        _buildButton(context, _responses[1]),
        _buildButton(context, _responses[2]),
      ],
    );
  }

  Widget _buildButton(BuildContext context, String text) {
    return Container(
        margin: EdgeInsets.only(left: _margin, right: _margin),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.disabled))
                  return Colors.white70;
                return Colors.white12;
              },
            ),
          ),
          onPressed: _error.contains(text) ? null : () => _onClick(text),
          child: Text(text,
              style: TextStyle(
                  color: _error.contains(text) ? Colors.red : Colors.white,
                  fontWeight: FontWeight.bold)),
        ));
  }
}
