del *.class /Q /S
del *.log /Q /S
del *.pyc /Q /S
del *.swp /Q /S
del .DS_Store
rmdir .atom /S /Q
rmdir .buildlog /S /Q
rmdir .history /S /Q
rmdir .svn /S /Q

del *.iws /Q /S
del *.iml /Q /S
del *.ipr /Q /S
rmdir .idea /S /Q

rmdir .dart_tool /S /Q
del .flutter-plugins /Q
del .flutter-plugins-dependencies /Q
del .packages /Q 
rmdir .pub-cache /Q /S
rmdir .pub /Q /S
rmdir build /Q /S